"""
setup.py for lhc-longitudinal-tomography-ml-ucap.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages


HERE = Path(__file__).parent.absolute()
ressource_path = Path(__file__).parent.absolute()

with (HERE / 'README.md').open('rt') as fh:
    LONG_DESCRIPTION = fh.read().strip()


REQUIREMENTS: dict = {
    'core': [
        'ucap',
        'mlp_lhc_tomography',
        'numpy',
        'matplotlib',
    ],
    'test': [
        'pytest',
    ],
    'dev': [
    ],
    'doc': [
        # 'sphinx',
        # 'acc-py-sphinx',
    ],
}


setup(
    name='lhc-longitudinal-tomography-ml-ucap',
    version="0.1.2",

    author='gtrad',
    author_email='georges.trad@cern.ch',
    description='SHORT DESCRIPTION OF PROJECT',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    url='',

    packages=find_packages(),
    python_requires='>=3.6, <4',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
    package_data={'': [str(HERE/ 'lhc_longitudinal_tomography_ml_ucap/ml_data') +'/*.pk']},
    include_package_data=True,
)

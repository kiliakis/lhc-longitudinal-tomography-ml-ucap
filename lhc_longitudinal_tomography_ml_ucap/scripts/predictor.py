# import tensorflow as tf
import numpy as np
from mlp_lhc_tomography import LHCTomoscope
from mlp_lhc_tomography.utils import minmax_normalize_param, unnormalize_params
from mlp_lhc_tomography.utils import correctForTriggerOffset
from mlp_lhc_tomography.utils import calc_bin_centers

from mlp_client import Client, Profile, AUTO


class Predictor:
    def __init__(self, normalization_factors, scope_to_model_params):
        client = Client(Profile.PRO)
        encDec = client.create_model(
            model_class=LHCTomoscope,
            params_name='mlp_lhc_tomography.LHCTomoscope',
            params_version=AUTO,
            verbose=True
        )
        self.encDec = encDec
        self.normalization_factors = normalization_factors
        self.scope_to_model = scope_to_model_params


    def fromScopeToModelImg(self, scopeData):
        """_summary_

        Args:
            scopeData (_type_): An array of shape (100, >=300). Raw input data.

        Returns:
            _type_: Waterfall normalized, shape: (IMG_OUTPUT_SIZE, IMG_OUTPUT_SIZE, 1)
        """
        IMG_OUTPUT_SIZE = self.encDec.model.inputShape[0]
        start_turn = 1
        skipturns = 3
        centroid_offset = 0
        zeropad = 14
        Ib = self.scope_to_model['Ib']
        cut_left = self.scope_to_model['cut_left']
        cut_right = self.scope_to_model['cut_right']
        n_slices = self.scope_to_model['n_slices']
        corrTriggerOffset = self.scope_to_model['corrTriggerOffset']
        filter_n = self.scope_to_model['filter_n']
        iterations = self.scope_to_model['iterations']

        sel_turns = np.arange(
            start_turn, skipturns * (IMG_OUTPUT_SIZE - 2 * zeropad), skipturns).astype(np.int32)
        scopeData = scopeData / np.sum(scopeData, axis=0) * Ib
        timeScale_for_tomo = calc_bin_centers(cut_left, cut_right, n_slices)
        if corrTriggerOffset:
            scopeData = correctForTriggerOffset(timeScale_for_tomo, scopeData,
                                                filter_n=filter_n, iterations=iterations)

        norm_img = scopeData / self.normalization_factors['T_normFactor']
        norm_img = norm_img[:, sel_turns]
        norm_img = np.pad(norm_img, ((zeropad-centroid_offset, zeropad +
                                                    centroid_offset), (zeropad, zeropad)),
                                                     'constant',
                                                     constant_values=(0, 0))
        norm_img = np.reshape(norm_img, norm_img.shape + (1,))

        return norm_img

    def predictTurn(self, norm_wf, turn):
        """Predict Phasespace and latents for a given waterfall at a given turn

        Args:
            norm_wf (_type_): Normalized WF
            turn (_type_): Not-normalized turn (should be in the range 1-300)

        Returns:
            _type_: The PS unnormalized and the latents unnormalized
        """
        turn_min, turn_max = self.normalization_factors['turn_min_max']
        assert turn >= turn_min and turn <= turn_max
        norm_turn = minmax_normalize_param(turn, turn_min, turn_max)
        norm_turn = np.array([norm_turn])
        norm_wf = norm_wf.reshape( (1,) + norm_wf.shape)
        # print(norm_wf.shape, norm_turn.shape)
        inputs = {'inputs': norm_wf, 'turns': norm_turn}
        outputs = self.encDec.predict(inputs)
        
        norm_PS = np.array(outputs['outputs'])
        norm_latents = np.array(outputs['latents'])
        
        unnorm_PS = norm_PS * self.normalization_factors['B_normFactor']
        unnorm_latents = unnormalize_params(
                    norm_latents[:, 0], norm_latents[:, 1], norm_latents[:, 2],
                    norm_latents[:, 3], norm_latents[:, 4], norm_latents[:, 5],
                    norm_latents[:, 6], normalization='minmax')
        return unnorm_PS, unnorm_latents


    def predictAllTurns(self, norm_wf, n_turns=28):
        """Predict phasespace for all turns for a given WF

        Args:
            norm_wf (_type_): Normalized WF

        Returns:
            _type_: The PSs unnormalized and the latents unnormalized
        """
        turn_min, turn_max = self.normalization_factors['turn_min_max']

        assert n_turns > 0 and n_turns <= turn_max
        turns = np.linspace(turn_min, turn_max, num=n_turns, endpoint=True, dtype=int)
        norm_turn = minmax_normalize_param(turns, turn_min, turn_max)
        norm_wf = np.array([norm_wf] * len(turns))

        inputs = {'inputs': norm_wf, 'turns': norm_turn}
        outputs = self.encDec.predict(inputs)
        
        norm_PS = np.array(outputs['outputs'])
        norm_latents = np.array(outputs['latents'])
        
        unnorm_PS = norm_PS * self.normalization_factors['B_normFactor']
        unnorm_latents = unnormalize_params(
                    norm_latents[:, 0], norm_latents[:, 1], norm_latents[:, 2],
                    norm_latents[:, 3], norm_latents[:, 4], norm_latents[:, 5],
                    norm_latents[:, 6], normalization='minmax')
        return unnorm_PS, unnorm_latents, turns

from typing import Optional
import numpy as np
import logging

import os
import json
import pickle as pk

from datetime import datetime
from ucap.common import AcquiredParameterValue, FailSafeParameterValue, ValueType
from ucap.common.context import published_parameter_name, configuration
from ucap.common.parameter_exception import ParameterException
from lhc_longitudinal_tomography_ml_ucap.scripts.predictor import Predictor

# configuration = json.load(open('lhc_longitudinal_tomography_ml_ucap/definitions/lhc_long_tomo_ml.UCAP.json'))
# configuration = configuration['transformations'][0]['converter']['configuration']logger = logging.getLogger(__name__)

logger = logging.getLogger(__name__)
predictor = None


def configuration_post_processing():
    global predictor
    if configuration['model_id'] not in configuration['implemented_model_versions']:
        raise ValueError(f"Invalid model Id: {configuration['model_id']}")
    try:
        logger.info(configuration)
        logger.info('norm factors')
        logger.info(configuration['normalization_factors'])
        predictor = Predictor(configuration['normalization_factors'], 
                              configuration['scope_to_model_params'])
        logger.info('successfully created predictor')
    except:
        logger.error('Could not instantiate loaded model')


def convert(input_data: FailSafeParameterValue) -> Optional[FailSafeParameterValue]:
    logger.info(input_data)
    global predictor

    if input_data.exception:
        ex: ParameterException = ParameterException(published_parameter_name, input_data.header)
        return FailSafeParameterValue(ex)

    # input_value: float = input_data.value.get_value()
    input_value = input_data.value.get_value()

    # data_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'ml_data')
    # scope_data = pk.load(open(os.path.join(data_dir, 'scope_data_ucap.pk'), 'rb'))['scope_data']

    scope_data = np.array(input_value)


    logger.info('converting scope to model image')
    norm_img = predictor.fromScopeToModelImg(scope_data)
    logger.info('converted scope to image!')

    logger.info('using predictor to predict')
    turn = np.random.randint(1, 298)
    PS_img, latents = predictor.predictTurn(norm_img, turn)
    logger.info('succesfully predicted')
    logger.info(latents)

    result: AcquiredParameterValue = AcquiredParameterValue(published_parameter_name, input_data.header)
    result.update_value('scope_data', input_value, ValueType.FLOAT_ARRAY_2D)
    result.update_value('time_now', datetime.now().isoformat(), ValueType.STRING)
    result.update_value('PD_turn', int(turn), ValueType.INT)
    result.update_value('predicted_phase_spaces', PS_img.tolist(), ValueType.FLOAT_ARRAY_2D)
    result.update_value('predicted_parameters_values', list(latents), ValueType.FLOAT_ARRAY)

    return FailSafeParameterValue(result)

